import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SchoollistPage } from '../schoollist/schoollist';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  slides = [
    {
      //title: "School Name1",
    //  description: "The <b>Ionic Component Documentation</b> showcases a number of useful components that are included out of the box with Ionic.",
      image: "assets/imgs/s1.jpg",
    },
    {
      //title: "School Name2",
     // description: "<b>Ionic Framework</b> is an open source SDK that enables developers to build high quality mobile apps with web technologies like HTML, CSS, and JavaScript.",
      image: "assets/imgs/s2.jpg",
    },
    {
      //title: "School Name3 ",
     // description: "The <b>Ionic Cloud</b> is a cloud platform for managing and scaling Ionic apps with integrated services like push notifications, native builds, user auth, and live updating.",
      image: "assets/imgs/s3.jpg",
    }
  ];
  constructor(public navCtrl: NavController) {

  }
  schoolPage() {
    this.navCtrl.push(SchoollistPage);
  }

}
