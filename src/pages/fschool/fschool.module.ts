import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FschoolPage } from './fschool';

@NgModule({
  declarations: [
    FschoolPage,
  ],
  imports: [
    IonicPageModule.forChild(FschoolPage),
  ],
})
export class FschoolPageModule {}
