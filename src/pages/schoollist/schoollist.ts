import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FschoolPage } from '../fschool/fschool';

/**
 * Generated class for the SchoollistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schoollist',
  templateUrl: 'schoollist.html',
})
export class SchoollistPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  fschoolGo() {
    this.navCtrl.push(FschoolPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SchoollistPage');
  }

}
