import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SchoollistPage } from './schoollist';

@NgModule({
  declarations: [
    SchoollistPage,
  ],
  imports: [
    IonicPageModule.forChild(SchoollistPage),
  ],
})
export class SchoollistPageModule {}
