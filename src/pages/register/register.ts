import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../home/home';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  @ViewChild('username') uname;
  @ViewChild('password') password;

  constructor(private fire:AngularFireAuth, private AlertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }

  registerUser() {
    this.fire.auth.createUserWithEmailAndPassword(this.uname.value, this.password.value)
    .then(data => {
      console.log('got some data', data);
      this.alert('Registered' );
      this.navCtrl.setRoot(HomePage);
    })
    .catch(error =>{
      console.log('got an error', error);
      this.alert(error.message);
    });
    console.log('would login with user ', this.uname.value, this.uname.value, this.password.value)
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  alert(message: string) {
    this.AlertCtrl.create({
      title: 'WELCOME',
      subTitle: 'Registered successful',
      buttons: ['OK']
    }).present();
  }
}
