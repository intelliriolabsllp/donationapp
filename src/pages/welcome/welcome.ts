import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';


/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {
  
  @ViewChild('username') uname;
  @ViewChild('password') password;

  constructor(private fire:AngularFireAuth, private AlertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
    }
  
    logInUser(){
      this.fire.auth.signInWithEmailAndPassword(this.uname.value, this.password.value)
      .then(data => {
        console.log('got some data', this.fire.auth.currentUser);
        this.alert('Success ! you\'re logged in' );
        this.navCtrl.setRoot(TabsPage);
        //user is loged in 
      })
      .catch(error => {
        console.log('got an error', error);
      })
      
      console.log('would login with user ', this.uname.value, this.password.value);
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

  alert(message: string) {
    this.AlertCtrl.create({
      title: 'WELCOME',
      subTitle: 'Success ! You are logged In',
      buttons: ['OK']
    }).present();
  }

  Register() {
    this.navCtrl.push(RegisterPage);
  }
}
