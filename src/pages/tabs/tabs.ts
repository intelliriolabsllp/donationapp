import { Component } from '@angular/core';

import { DonationPage } from '../donation/donation';
import { ProfilePage } from '../profile/profile';
import { HomePage } from '../home/home';
import { SchoollistPage } from '../schoollist/schoollist';
import { WelcomePage } from '../welcome/welcome';
import { LogoutPage } from '../logout/logout';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = DonationPage;
  tab3Root = ProfilePage;
  tab4Root =  WelcomePage;
  tab5Root =  LogoutPage;

  constructor() {

  }
}
