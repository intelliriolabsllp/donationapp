import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { DonationPage } from '../pages/donation/donation';
import { ProfilePage } from '../pages/profile/profile';
import { HomePage } from '../pages/home/home';
import { SchoollistPage } from '../pages/schoollist/schoollist';
import { WelcomePage } from '../pages/welcome/welcome';
import { FschoolPage } from '../pages/fschool/fschool';
import { LogoutPage } from '../pages/logout/logout';
import { RegisterPage } from '../pages/register/register';
import { TabsPage } from '../pages/tabs/tabs';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';

const firebaseAuth = {
  apiKey: "AIzaSyDufZjARhwTM6693mYMPasxju-WPNw_xY8",
  authDomain: "donation-3949e.firebaseapp.com",
  databaseURL: "https://donation-3949e.firebaseio.com",
  projectId: "donation-3949e",
  storageBucket: "donation-3949e.appspot.com",
  messagingSenderId: "751906083660"
};

@NgModule({
  declarations: [
    MyApp,
    DonationPage,
    ProfilePage,
    HomePage,
    SchoollistPage,
    WelcomePage,
    FschoolPage,
    LogoutPage,
    RegisterPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAuth),
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DonationPage,
    ProfilePage,
    HomePage,
    SchoollistPage,
    WelcomePage,
    FschoolPage,
    LogoutPage,
    RegisterPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
